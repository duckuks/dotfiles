-- Maximizes and restores the current window in Vim.
-- https://github.com/szw/vim-maximizer/tree/master

return {
  'szw/vim-maximizer',
  keys = {
    { '<leader>sm', '<cmd>MaximizerToggle<CR>', desc = 'Maximize/minimize a split' },
  },
}
